var express = require('express');
var app = express();

// app.use(express.static(__dirname));

app.configure(function(){
  app.use(function(req, res, next) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    return next();
  });
  app.use(express.static(__dirname));
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.listen(process.env.PORT || 3000);
