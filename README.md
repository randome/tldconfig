tldConfig
=========

tldConfig is a tool for Ascio to setup and manage TLD configurations on different platforms.

## Features
- Manage TLDs
- Manage validation rules
- Manage XSLT templates
- Manage scripts

## Requirements
- [nodejs](http://nodejs.org/) 0.10 +
- [brunch.io](http://brunch.io) 1.5 +
- Ruby 1.9

## Getting Started

1. Install brunch
`npm install -g brunch`

2. Run the server
`brunch w -s`

3. Open [http://localhost:3333/](http://localhost:3333/)

4. See tests [http://localhost:3333/test/](http://localhost:3333/test/)

## Using
- Backbone
- jQuery
- Lo-Dash
- Marionette
- Handlerbars
- Stylus
- Mocha with Chai

## Author
[Dejan Stokanic](http://stokanic.eu)