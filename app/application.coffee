class Application extends Backbone.Marionette.Application

  initialize: =>
    @addRegions
      headerRegion: "#topbar"
      sidebarRegion: "#sidebar"
      tldListRegion: "#tld-list"
      mainRegion: "#main-content"
      toolbarRegion: "aside#toolbar"


    # require all the modules here
    @on("initialize:before", (options) ->
      require 'modules/tld/tld_module'
      require 'modules/sidebar/sidebar_module'
      require 'modules/header/header_module'
      require 'modules/tldList/tldList_module'
    )

    @addInitializer ->
      @reqres.setHandler "get:default:region", =>
        @mainRegion

      @reqres.setHandler "get:toolbar:region", =>
        @toolbarRegion

    @addInitializer ->
      #@module("Header").start()

      @module("Sidebar").start
        region: @sidebarRegion

      @module("Tld").start
        region: @mainRegion

      @module("TldList").start
        region: @tldListRegion

    @on("initialize:after", (options) =>
      rivetsConfig = require 'config/rivets'
      rivets.configure rivetsConfig

      Backbone.history.start()

      require 'config/marionette'

      # make the Application object immutable
      Object.freeze? this
    )

    @start()

module.exports = new Application()
