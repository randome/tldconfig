_baseUrl =  "/"
_baseAPIUrl = "http://localhost:3000/"
_version = "v0.4.1"
_gravatarUrl = "http://www.gravatar.com/avatar/"
_gravatarSize = 32

AppConfig =
  BASE_URL : _baseUrl
  BASE_API_URL : _baseAPIUrl
  VERSION : _version
  GRAVATAR_URL : _gravatarUrl
  GRAVATAR_SIZE : _gravatarSize

module.exports = AppConfig
