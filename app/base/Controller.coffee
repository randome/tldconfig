App = require 'application'

class Controller extends Marionette.Controller
  constructor: (options = {}) ->
    @region = options.region or App.request "get:default:region"
    super options

  show: (view) ->
    @currentView = view
    @region.show view

module.exports = Controller