class ToolbarView extends Marionette.ItemView
  template: require './template/toolbar'
  className: "toolbar"
  buttons:
    left: []
    right: []

  serializeData: ->
    @buttons


module.exports = ToolbarView