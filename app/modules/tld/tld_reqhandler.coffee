App = require 'application'

App.module "Tld.ReqHandler", (ReqHandler, App, Backbone) ->
  Tld = require './models/Tld'
  Tlds = require './models/Tlds'


  ReqHandler.Handlers =

    initialize: ->
      App.reqres.setHandler "get:tld", (name, onSuccess) => 
        @getTld name, onSuccess

      App.reqres.setHandler "get:tlds", @getTlds

      App.reqres.setHandler "get:empty:tld", @getEmptyTld

      App.commands.setHandler "save:tld", (tld) =>
        @saveTld tld

    getTld: (name, onSuccess) ->
      tld = new Tld
        "id": name

      tld.fetch
        success: ->
          onSuccess tld

    getTlds: ->
      tlds = new Tlds
      tlds.fetch()
      tlds

    saveTld: (tld) ->
      tld.save()

    getEmptyTld: ->
      emptyTld = new Tld

  ReqHandler.addInitializer ->
    @Handlers.initialize()