App = require 'application'

App.module "Tld", (Tld, App, Backbone, Marionette) ->

  @startWithParent = false

  Tld.addInitializer (options) ->
    throw "region not specified" if !options || !options.region
    require './tld_reqhandler'
    require './tld_controller'

    TldRouter = require './tld_router'
    router = new TldRouter
              controller: new Tld.Controller options

    # this should not be here, nothing happens if the module is not started
    router.on "route", (action, params) ->
      App.trigger "navigate:route", "tld:" + action