App = require 'application'
Toolbar = require 'components/toolbar/Toolbar'

App.module "Tld.View.Edit", (Edit, App, Backbone, Marionette) ->
  class Edit.ToolbarView extends Toolbar
    buttons:
      left: [
        {
          "className" : "back"
          "icon"      : "arrow-left"
          "text"      : "Back"
        }
      ]

    events:
      "click button.back" : -> App.navigate "tld"

  class Edit.View extends Marionette.ItemView
    template: require './templates/tld_edit'
    tagName: "section"
    className: "aui-page-fixed"
    attributes:
      "id" : "content"
      "role" : "main"

    events:
      "click button#save-btn" : "saveTld"

    onDomRefresh: ->
      rivets.bind @el, {model: @model}

    saveTld: ->
      App.commands.execute "save:tld", @model