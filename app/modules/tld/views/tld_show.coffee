App = require 'application'
Toolbar = require 'components/toolbar/Toolbar'

App.module "Tld.View.Show", (Show, App, Backbone, Marionette) ->

  class Show.ToolbarView extends Toolbar
    buttons:
      left: [
        {
          "className": "new"
          "icon" : "plus"
          "text" : "New Tld"
        }
        {
          "className": "edit"
          "icon" : "edit"
          "text" : "Edit Tld"
        }
      ]
      right: [
        {
          "className": "delete"
          "icon": "trash"
          "text": "Delete"
        }
        {
          "className": "clone"
          "icon": "copy"
          "text": "Clone"
        }
      ]

    events:
      "click button.new"    : -> App.navigate "tld/add"
      "click button.edit"   : -> App.navigate App.getCurrentRoute() + "/edit"
      "click button.clone"  : -> App.navigate "tld/add"
      "click button.delete" : "deleteTld"

    deleteTld: ->
      console.log "Create confirm dialog"

  class Show.View extends Marionette.ItemView
    template: require './templates/tld_show'
    tagName: "section"
    className: "aui-page-fixed"
    attributes:
      "id" : "content"
      "role" : "main"

    events:
      "click button#provider-link": "addProviderLink"

    initialize: ->
      Handlebars.registerPartial "tld_provider_link",
        require './templates/partials/tld_show/provider_link'

      Handlebars.registerPartial "tld_renewal",
        require './templates/partials/tld_show/renewal'

      Handlebars.registerPartial "tld_setup",
        require './templates/partials/tld_show/setup'

    tldEdit: ->
      App.navigate "tld/" + @model.id + "/edit"

    tldClone: (e) ->
      console.log "Clone the tld"
      App.navigate "tld/add"#, @model
      e.preventDefault()

    tldDelete: (e) ->
      console.log "Delete this tld"
      e.preventDefault()

    addProviderLink: (e) ->
      e.preventDefault()

      dialog = new AddProviderLinkDialog()
      dialog.render()

      #console.log e.target
      $(e.target).popover
        html: true
        content: dialog.el
        placement: "bottom"
        title: "Provider Link"


  class AddProviderLinkDialog extends Marionette.ItemView
    template: require './templates/partials/tld_show/add_provider_link'

    events:
      "click button#add" : "addProviderLink"
      "click button#new" : "newProviderLink"

    addProviderLink: (e) ->
      console.log "save prodvider link"
      @close()

    newProviderLink: (e) ->
      console.log "open provider link modal"