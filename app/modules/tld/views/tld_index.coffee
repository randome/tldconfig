App = require 'application'
Toolbar = require 'components/toolbar/Toolbar'

App.module "Tld.View.Index", (Index, App, Backbone, Marionette) ->

  class Index.ToolbarView extends Toolbar
    buttons:
      left: [
        {
          "className": "new"
          "icon" : "plus"
          "text" : "New Tld"
         }
      ]

    events:
      "click button.new"  :  -> App.navigate "tld/add"

  class Index.View extends Marionette.ItemView
    template: require './templates/tld_index'
    tagName: "section"
    attributes:
      "id" : "content"
      "role" : "main"

    events:
      "click button" : -> App.navigate "tld/add"
