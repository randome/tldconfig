App = require 'application'
Toolbar = require 'components/toolbar/Toolbar'

App.module "Tld.View.Add", (Add, App) ->

  class Add.ToolbarView extends Toolbar
    buttons:
      left: [
        {
          "className" : "back"
          "icon"      : "arrow-left"
          "text"      : "Back"
        }
      ]

    events:
      "click button.back" : -> App.navigate "tld"

  class Add.View extends Marionette.ItemView
    template: require './templates/tld_add'
    tagName: "section"
    className: "aui-page-fixed"
    attributes:
      "id" : "content"
      "role" : "main"

    events:
      "click button#add-btn": "addTld"

    initialize: ->
      @model.on "change:tld.S_TLD", @render

    onDomRefresh: ->
      rivets.bind @el, {model: @model}

    addTld: ->
      console.log @model.toJSON()
      console.log "adding tld"