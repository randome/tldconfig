App = require 'application'
ControllerBase = require 'base/Controller'

App.module "Tld", (Tld, App, Backbone, Marionette) ->

  Tld.addInitializer ->
    require './views/tld_show'
    require './views/tld_edit'
    require './views/tld_add'
    require './views/tld_index'

  class Controller extends ControllerBase
    initialize: ->
      @toolbarRegion = App.request "get:toolbar:region"

    index: ->
      indexView = new Tld.View.Index.View

      @toolbarRegion.show new Tld.View.Index.ToolbarView
      @show indexView

    view: (tld) ->
      App.request "get:tld", tld, (tldModel) =>
        @toolbarRegion.show new Tld.View.Show.ToolbarView
        showView = new Tld.View.Show.View
          model: tldModel
        @show showView

    edit: (tld) ->
      App.request "get:tld", tld, (tldModel) =>
        @toolbarRegion.show new Tld.View.Edit.ToolbarView
        editView = new Tld.View.Edit.View
          model: tldModel
        @show editView

    add: (tld) ->
      @toolbarRegion.show new Tld.View.Add.ToolbarView
      newTld = App.request "get:empty:tld"
      addView = new Tld.View.Add.View
        model: newTld
      @show addView

  Tld.Controller = Controller