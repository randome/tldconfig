Config = require "config/appConfig"

class Tld extends Backbone.DeepModel
  urlRoot: Config.BASE_API_URL + "tld"

module.exports = Tld
