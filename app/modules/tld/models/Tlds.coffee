Config = require "config/appConfig"
Tld = require './Tld'

class Tlds extends Backbone.Collection
  url: Config.BASE_API_URL + "tlds.json"
  model: Tld

module.exports = Tlds
