class TldRouter extends Marionette.AppRouter
  appRoutes:
    "tld": "index"
    "tld/add": "add"
    "tld/:tld": "view"
    "tld/:tld/edit": "edit"

module.exports = TldRouter