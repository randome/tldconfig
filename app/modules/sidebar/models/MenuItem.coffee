class MenuItem extends Backbone.Model

class MenuItemCollection extends Backbone.Collection
  model: MenuItem

module.exports = MenuItemCollection