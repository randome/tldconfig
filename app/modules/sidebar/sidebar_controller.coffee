App = require 'application'
Controller = require 'base/Controller'

App.module "Sidebar", (Sidebar, App) ->

  class Controller extends Controller
    initialize: ->
      App.on "navigate:route", @onNavigateRoute, this

      SidebarView = require './views/sidebar_view'
      @links = App.request "get:menu:items"

      @show new SidebarView collection: @links

    onNavigateRoute: (route) ->
      route = route.split(":")[0]
      item = @links.findWhere { url: route }
      @currentView.trigger "navigate:route", item


  Sidebar.Controller = Controller