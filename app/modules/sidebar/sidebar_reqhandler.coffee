App = require 'application'
MenuItemsCollection = require './models/MenuItem'

App.module "Sidebar.ReqHandler", (ReqHandler, App) ->

  ReqHandler.Handlers =
      getMenuItems: ->
        new MenuItemsCollection [
          {
            name: "Dashboard"
            url: "dashboard"
            icon: "dashboard"
          }
          {
            name: "Product Configration"
            url: "productcontext"
            icon: "inbox"
          }
          {
            name: "Tld Configration",
            url: "tld",
            icon: "globe"
          }
          {
            name: "Validation Rules",
            url: "validation",
            icon: "check"
          }
          {
            name: "Template Configration",
            url: "tempaltes",
            icon: "code"
          }
          {
            name: "Accounts"
            url: "accounts",
            icon: "user"
          }
          {
            name: "Settings"
            url: "settings"
            icon: "cog"
          }
        ]

  App.reqres.setHandler "get:menu:items", ->
    ReqHandler.Handlers.getMenuItems()
