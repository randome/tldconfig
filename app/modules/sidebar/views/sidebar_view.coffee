App = require 'application'


#class SidebarNav extends Marionette.CompositeView

class SidebarItem extends Marionette.ItemView
  template: require './templates/menuItem'
  tagName: "li"
  events:
    "click": -> @trigger "selected", this

class SidebarView extends Marionette.CompositeView
  template: require './templates/sidebar'
  itemView: SidebarItem
  itemViewContainer: "ul.nav"
  className: "inner"

  initialize: ->
    @on "itemview:selected", @onItemViewSelected

    @on "navigate:route", (item) ->
      itemView = @children.findByModel(item)
      @onItemViewSelected itemView

  onItemViewSelected: (itemView) ->
    @$el.find(".active").removeClass("active")
    itemView.$el.addClass("active")

module.exports = SidebarView