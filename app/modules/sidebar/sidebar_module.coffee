App = require 'application'

App.module "Sidebar", (Sidebar, App) ->

  @startWithParent = false

  Sidebar.addInitializer ->
    require './sidebar_reqhandler'
    require './sidebar_controller'

  API =
    showSidebar: (options) ->
      new Sidebar.Controller options

  Sidebar.on "start", (options) ->
    API.showSidebar options