App = require 'application'
Controller = require 'base/Controller'

App.module "TldList", (TldList, App) ->

  class Controller extends Controller
    initialize: ->
      TldListView = require './views/tldList_view'

      tlds = App.request "get:tlds"

      @show new TldListView collection: tlds

  TldList.Controller = Controller