App = require 'application'

App.module "TldList", (TldList, App) ->
  @startWithParent = false

  TldList.addInitializer ->
    require './tldList_controller'

  API =
    showTldList: (options) ->
      new TldList.Controller options


  TldList.on "start", (options) ->
    API.showTldList options
