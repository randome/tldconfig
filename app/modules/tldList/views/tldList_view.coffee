App = require 'application'

class TldListItem extends Marionette.ItemView
  template: require './templates/tldListItem'
  tagName: "li"
  className: "tld"
  events:
      "click": -> @trigger "selected", this

  removeSelected: ->
    @$el.removeClass "selected"

class TldListView extends Marionette.CompositeView
  template: require './templates/tldList'
  itemView: TldListItem
  itemViewContainer: "ul.tlds"
  className: "inner"

  initialize: ->
    @on "itemview:selected", (item) ->
      # todo : see which option is faster
      # @children.call "removeSelected"
      @$el.find(".selected").removeClass("selected")
      item.$el.addClass "selected"
      App.navigate "tld/" + item.model.get('tld')

module.exports = TldListView
