App = require 'application'

App.module "Header.Controller", (Controller, App, Backbone, Marionette) ->

  Controller.addInitializer ->
    require './views/header_view'

  # sign up for application wide events
  Controller.addInitializer ->
    #App.on "navigate:route", Events.onNavigateRoute, this

  Events =
    onNavigateRoute: (route) ->
      route = route.split(":")[0]
      menuItem = menuItems.findWhere { url : route }
      App.Header.View.trigger "navigate:route", menuItem

  menuItems = {}

  Controller.Actions =
    show: ->
      user = App.request "get:current:user"
      config = require 'config/appConfig'
      view = new App.Header.View.HeaderView
        config: config
        user: user

      App.headerRegion.show view