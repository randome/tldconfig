App = require 'application'

App.module "Header.ReqHandler", (ReqHandler, App) ->

  ReqHandler.Handlers =

    #assumig we'll use admin premissions one day..
    getSuperUserMenuItems: ->
      new MenuItemsCollection [
        {name: "Accounts", url : "accounts"}
        {name: "User Rights", url : "user_rights"}
      ]

    getCurrentUser: ->
      user =
        username: "Dejan Stokanic"
        gravatar_hash: "569a24a030abe51b84c4fd0331efefc2"

  App.reqres.setHandler "get:current:user", ->
    ReqHandler.Handlers.getCurrentUser()