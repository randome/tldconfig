App = require 'application'

App.module "Header.View", (View, App, Backbone, Marionette, $) ->

  class View.MenuItem extends Marionette.ItemView
    template: require './templates/menuItem'
    tagName: "li"
    events:
      "click" : ->
        View.trigger "selected:menu:item", this

    removeClass: ->
      @el.className = ""

  class View.Navbar extends Marionette.CompositeView
    template: require './templates/navbar'
    itemView: View.MenuItem
    itemViewContainer: "ul.aui-nav"
    className: "aui-navgroup-primary"

    initialize: ->
      View.on "selected:menu:item", @onSelectedMenuItem, this
      View.on "navigate:route", @onNavigateRoute, this

    onSelectedMenuItem: (menuItemView) ->
      @children.call("removeClass")
      menuItemView.el.className = "aui-nav-selected"

    onNavigateRoute: (menuItem) ->
      menuItemView = @children.findByModel(menuItem)
      @onSelectedMenuItem(menuItemView)

  class View.UserMenuItem extends Marionette.ItemView
    template: require './templates/user/userMenuItem'
    tagName: 'li'

  class View.UserMenu extends Marionette.CompositeView
    template: require './templates/user/userMenu'
    itemView: View.UserMenuItem
    itemViewContainer: "ul#user-menu"

  class View.HeaderView extends Marionette.Layout
    template: require './templates/header_layout'
    className: "layout"
    regions:
      navbarRegion: "nav .container"
      userMenuRegion: ".dropdown-menu"

    serializeData: ->
      {
        config: @options.config
        user: @options.user
      }

    onRender: ->
      NavbarView = new View.Navbar
        collection: @collection

      #@navbarRegion.show NavbarView
      #@UserMenuView = new View.UserMenu collection: #?