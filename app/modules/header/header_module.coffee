# Header module that will take care of the topbar with the usermenu
# and main navigation
App = require 'application'

App.module "Header", (Header) ->
  @startWithParent = false

  Header.addInitializer ->
    require './header_reqhandler'
    require './header_controller'

  API =
    show: ->
      Header.Controller.Actions.show()

  Header.on "start", ->
    API.show()