tests = [
  'modules/header_module'
  #'modules/tld_module'  
]

window.assert = chai.assert
window.expect = chai.expect

mocha.setup 
  ui: 'bdd'
  globals: ['App', 'jQuery']
  
mocha.reporter 'html'

for test in tests
  test_file = 'test/' + test + '_test'
  console.log "Loading", test_file
  require test_file